<?php $title = 'About'; include 'header.php'; ?>

<!-- Page picture -->
<div class="page-header no-margin no-padding bottom-margin-60px">
    <div class="container-fluid no-margin no-padding">
        <img src="images/about_image.jpg" class="img-responsive">
    </div>
</div><!-- End of Page picture -->


<div id="about_nsbm" class="container font_and_height">
    <h2>About NSBM</h2>
    <img src="images/nsbm.jpg" height="500" class="pull-left margin-right-bottom-40px">
    <p>The National School of Business Management (NSBM)
        is the degree school of National Institute of
        Business Management (NIBM) and offers undergraduate
        and postgraduate degree programs in Management,
        Computing and Engineering. Innovation is the key
        at NSBM as we understand the importance of originality
        and creativity in this global world. The education
        system at NSBM is therefore designed with the intention
        of ensuring that all our undergraduates are fully prepared
        to face any challenges the world has to offer.</p>

    <p>We pride ourselves on the innovative and entrepreneurship
        qualities that NSBM imparts to its undergraduates through
        its excellence in teaching and research. Along with our
        ethnically and culturally diverse workforce we also inculcate
        International expertise in the fields of Management, Computing
        and Engineering in partnership with leading universities in
        the world. NSBM offers world renowned degrees that are much more
        than just a certificate. Our lecture panel skillfully integrates
        leadership, ethics, global thinking, core management skills
        and technological innovations into the learning process to
        make our undergraduates well rounded global citizens.</p>

    <p>Our undergraduates enjoy a healthy work-life balance,
        as NSBM encourages students to develop and pursue their
        talents and hobbies through various events and activities
        like the Annual Sports Day and the celebrations of
        multicultural festivals which showcase their creative
        and musical abilities.</p>

    <p>We are also the very first institute in Sri Lanka with green
        commitment. The modern green university premises of NSBM at
        Pitipana, Homagama will be completed by the end of 2015.
        The design of the construction is not only to generate
        surroundings which cater to the physical and physiological
        comfort of both students and academics, but also to create
        structures that would harmonize with the existing physical
        and natural environment, whilst being techno and ecofriendly,
        built with the latest technology and modern materials requiring
        a less maintenance effort. Thus we are a flexible school that
        adapts to the changes of the 21st century and in turn we help
        mould our students to be equally flexible and well versed in
        everything they choose to do. </p>

</div>

<div class="container">
    <div id="vision_and_mission" class="jumbotron center-align">
        <h2>Our Vision</h2>
        <p>To be the foremost engineering school to create future leaders</p>
        <h2>Our Mission</h2>
        <p>Contribute to building a prosperous society through education, learning and research</p>

    </div>
</div>

<div id="members" class="container">
    <h2>Group Members</h2>
    <div class="row font_and_height">
        <div class="col-sm-12 col-md-6 col-lg-6 left-right-padding-0px">
            <div class="col-sm-6 col-md-6 col-lg-6">
                <img src="images/nirmana.jpg" class="img-circle" alt="Cinque Terre" width="200" height="200">
                <br><br>Name: Nirmana Sankalpa<br>
                Index No: BSC-PLY-COM-15.1-076
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6">
                <img src="images/ashok.jpg" class="img-circle" alt="Cinque Terre" width="200" height="200">
                <br><br>Name: Ashok Perera<br>
                Index No: BSC-PLY-COM-15.1-077
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-6 left-right-padding-0px">
            <div class="col-sm-6 col-md-6 col-lg-6">
                <img src="images/chrishan.jpg" class="img-circle" alt="Cinque Terre" width="200" height="200">
                <br><br>Name: Chrishan Perera<br>
                Index No: BSC-PLY-COM-15.1-080
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6">
                <img src="images/duwane.jpg" class="img-circle" alt="Cinque Terre" width="200" height="200">
                <br> <br>Name: Duwane Wanigasinghe<br>
                Index No: BSC-PLY-COM-15.1-111
            </div>
        </div>
    </div>
</div>


<?php include 'footer.php'; ?>