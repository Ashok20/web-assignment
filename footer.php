<footer>
    <div id="footer_part1">
       <!-- <div id="footer_upper_section1">
            <div class="border"><i class="fa fa-facebook-official fa-2x"></i></div>
            <div class="border"><i class="fa fa-google-plus fa-2x"></i></div>
            <div class="border"><i class="fa fa-google-plus fa-2x"></i></div>
            <div class="border"><i class="fa fa-linkedin-square"></i></div>
        </div>-->
        <div class="upper_section" id="footer_upper_section2">
            <ul>
                <li><a href="/web-assignment/">HOME</a></li>
                <li><a href="/web-assignment/about.php">ABOUT</a></li>
                <li><a href="http://nsbm.lk/school-of-computing">COMPUTING</a></li>
                <li><a href="#">TERMS</a></li>
                <li><a href="/web-assignment/contact.php">CONTACT</a></li>
            </ul>
        </div>
        <div class="upper_section" id="footer_upper_section3">
            <ul>
                <li class="no_left_border"><a href="http://nsbm.lk/news">NEWS</a></li>
                <li><a href="http://nsbm.lk/library">LIBRARY</a></li>
                <li><a href="http://nsbm.lk/dress-code">DRESS</a></li>
                <li><a href="http://nsbm.lk/exams">EXAMS</a></li>
            </ul>
        </div>
    </div>
    <div id="footer_part2">
        <div id="footer_lower_section1">AN INVESTMENT IN KNOWLEDGE PAYS THE BEST INTEREST</div>
    </div>
</footer>

<script src="js/jQuery.js"></script><!-- jQuery File -->
<script src="js/bootstrap.min.js"></script><!-- Bootstrap JavaScript File -->

</body>
</html>
