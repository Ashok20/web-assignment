<?php $title = 'Contact'; include 'header.php'; ?>

<!-- Page picture -->
<div class="page-header no-margin no-padding bottom-margin-60px">
    <div class="container-fluid no-margin no-padding">
        <img src="images/contact_image.jpg" class="img-responsive">
    </div>
</div><!-- End of Page picture -->

<!-- Starting of Form -->
<div class="row">
    <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3 col-lg-8 col-lg-offset-2">
        <form class="form-horizontal" role="form" name="feedback_form" method="post" action="#" onsubmit="validate_text(event)">
            <div id="fname_division" class="form-group">
                <label class="control-label col-sm-3" for="feedback_fname">First Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="feedback_fname">
                    <span id="feedback_glyphicon_fname" aria-hidden="true"></span>
                    <span id="helpBlock_fname" class="help-block display_none"></span>
                </div>
            </div>
            <div id="lname_division" class="form-group">
                <label class="control-label col-sm-3" for="feedback_lname">Last Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="feedback_lname">
                    <span id="feedback_glyphicon_lname" aria-hidden="true"></span>
                    <span id="helpBlock_lname" class="help-block display_none"></span>
                </div>
            </div>
            <div id="email_division" class="form-group">
                <label class="control-label col-sm-3" for="feedback_email">Email</label>
                <div class="col-sm-9">
                    <input type="email" class="form-control" name="feedback_email">
                    <span id="feedback_glyphicon_email" aria-hidden="true"></span>
                    <span id="helpBlock_email" class="help-block display_none"></span>
                </div>
            </div>
            <div id="contact_division" class="form-group">
                <label class="control-label col-sm-3" for="feedback_contact">Contact Number</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="feedback_contact">
                    <span id="feedback_glyphicon_contact" aria-hidden="true"></span>
                    <span id="helpBlock_contact" class="help-block display_none"></span>
                </div>
            </div>
            <div id="comment_division" class="form-group">
                <label class="control-label col-sm-3" for="feedback_comment">Enter Your Message</label>
                <div class="col-sm-9">
                    <textarea class="form-control" rows="5" name="feedback_comment" placeholder="What can we help you with?"></textarea>
                    <span id="feedback_glyphicon_comment" aria-hidden="true"></span>
                    <span id="helpBlock_comment" class="help-block display_none"></span>
                </div>
            </div>
            <button type="submit" class="btn btn-primary btn-md btn pull-right" name="submit">Send Message</button>
            <button type="reset" class="btn btn-primary btn-md btn pull-right right-margin-10px" name="reset">Reset</button>
        </form>
    </div>
</div><!-- End of Form -->

<!-- Staring of contact details -->
<div id="contact_info" class="container">
    <div class="row">
        <div class="col-xs-8 col-xs-offset-2 col-sm-5 col-md-3 col-md-offset-0 col-lg-3 col-lg-offset-0">
            <ul>
                <li><h3>Engineering Faculty:</h3></li>
                <li>7, Station Road <br>Bambalapitiya <br>Colombo 4 <br>00400 <br>Sri Lanka <br>Tel:<a href=""> +94 (11) 250 7000</a> <br>Fax:<a href=""> +94 (11) 544 5009</a></li>
            </ul>
        </div>
        <div class="col-xs-8 col-xs-offset-2 col-sm-5 col-sm-offset-1 col-md-3 col-md-offset-0 col-lg-3 col-lg-offset-0">
            <ul>
                <li><h3>Computing Faculty:</h3></li>
                <li>7, Station Road <br>Bambalapitiya <br>Colombo 4 <br>00400 <br>Sri Lanka <br>Tel:<a href=""> +94 (11) 250 7001</a> <br>Fax:<a href=""> +94 (11) 544 5009</a></li>
            </ul>
        </div>
        <div class="col-xs-8 col-xs-offset-2 col-sm-5 col-md-3 col-md-offset-0 col-lg-3 col-lg-offset-0">
            <ul>
                <li><h3>α-C Library:</h3></li>
                <li>8, Station Road <br>Bambalapitiya <br>Colombo 4 <br>00400 <br>Sri Lanka <br>Tel:<a href=""> +94 (11) 250 7002</a> <br>Fax:<a href=""> +94 (11) 544 5009</a></li>
            </ul>
        </div>
        <div class="col-xs-8 col-xs-offset-2 col-sm-5 col-sm-offset-1 col-md-3 col-md-offset-0 col-lg-3 col-lg-offset-0">
            <ul>
                <li><h3>α-C Exam Unit:</h3></li>
                <li>7, Station Road <br>Bambalapitiya <br>Colombo 4 <br>00400 <br>Sri Lanka <br>Tel:<a href=""> +94 (11) 250 7003</a> <br>Fax:<a href=""> +94 (11) 544 5009</a></li>
            </ul>
        </div>
    </div>
</div><!-- End of contact details -->

<script src="js/contact_rules.js" ></script><!-- Contact Us Rules (General Rules) JavaScript File -->

<?php include 'footer.php'; ?>