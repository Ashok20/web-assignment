<?php include 'include.php'?>

<?php
$title = 'Home';
include 'header.php';
?>

<!-- Page picture -->
<div class="page-header no-margin no-padding bottom-margin-60px">
    <div class="container-fluid no-margin no-padding">
        <img src="images/index_image.jpg" class="img-responsive">
    </div>
</div><!-- End of Page picture -->

<div class="container">
    <div class="jumbotron">
        <div class="row font_and_height">
            <div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
                <p>We are a leading degree awarding institute approved by the University Grants Commission (UGC) under the Universities Act. We are members of the Association
                    of Commonwealth Universities (ACU), as well as the International Association of Universities (IAU). For more than 4 years, α-Computing university has been empowering
                    our student community in making a difference in their lives. Uniqueness of our courses have equipped our students to engage with the big issues of our time and embark
                    on truly rewarding careers. Through critical thinking and creative problem – solving our students are be ready to face the world.</p>
            </div>
            <div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
                <p>α-Computing was established in 2012. We opened our doors to 400 students in Metro Campus in Colombo. Currently, we offer both undergraduate and postgraduate courses,
                    and accommodate over 3000 students, including international students from various regions in the world. More than 9000 alumni have graduated from our three faculties:
                    Computer and Engineering. We take great pride in producing graduates who make meaningful contributions to their communities and professions. Among our diversely qualified
                    graduates alumni are software engineers and engineers. α-Computing attracts the largest share of private higher education students in Sri Lanka and our courses are designed
                    to equip graduates to succeed in contemporary workplaces with their exposure to practical (lab, internship etc) as well as the classroom environment. According to the
                    graduate Tracer Studies of 2014/2015 nearly 90% of our Bachelor degree graduates find full-time employment within six months of course completion. We are one of premier
                    degree awarding institute with the 25 acre site to provide the university exposure for our students.</p>
            </div>
            <div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
                <p>We are proud to announce that our industry internship programmes with leading multinational and local organizations in Sri Lanka have provided an opportunity for our students
                    to learn beyond the taught content, enhancing their professional personality, and provide them with a greater understanding about the practical application of their knowledge.</p>
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>