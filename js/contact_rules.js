/**
 * Created by ashok on 3/7/2016.
 */
function validate_text(event){
    if(document.feedback_form.feedback_fname.value.length<1){
        variable_identify('fname' , 'Enter Your First Name');
        event.preventDefault();
    }
    else {
        clss_name_correct('fname');
    }
    if(document.feedback_form.feedback_lname.value.length<1){
        variable_identify('lname' , 'Enter Your Last Name');
        event.preventDefault();
    }
    else {
        clss_name_correct('lname');
    }
    if(document.feedback_form.feedback_email.value.length<1){
        variable_identify('email' , 'Enter Your Email');
        event.preventDefault();
    }
    else {
        clss_name_correct('email');
    }
    if(document.feedback_form.feedback_contact.value.length<1){
        variable_identify('contact' , 'Enter Your Contact Number');
        event.preventDefault();
    }
    else {
        clss_name_correct('contact');
    }
    if(document.feedback_form.feedback_comment.value.length<1){
        variable_identify('comment' , 'Enter Your Comment');
        event.preventDefault();
    }
    else {
        clss_name_correct('comment');
    }

}
function variable_identify(name , help_block_text){
    class_name_error(name);
    document.getElementById('helpBlock_'+name).innerHTML = help_block_text;
    document.feedback_form['feedback_'+name].focus();
}
function class_name_error(name){
    document.getElementById(name+'_division').className = "form-group has-error has-feedback";
    document.getElementById('feedback_glyphicon_'+name).className = "glyphicon glyphicon-remove form-control-feedback";
    document.getElementById('helpBlock_'+name).className = "help-block font_sixe_11px";
}
function clss_name_correct(name){
    document.getElementById(name+'_division').className = "form-group has-success has-feedback";
    document.getElementById('feedback_glyphicon_'+name).className = "glyphicon glyphicon-ok form-control-feedback";
    document.getElementById('helpBlock_'+name).innerHTML = "";
}