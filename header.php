<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php if(isset($title)){} else{$title = 'ALPHA - C';} echo $title; ?></title>
    <link rel="icon" href="images/main_logo.png"><!-- Logo @ the Tab -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'><!-- Font family of the web -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/><!-- Bootstrap CSS File -->
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css"/><!-- Font-Awesome File -->
    <link rel="stylesheet" type="text/css" href="css/general_styles.css"/><!-- Additional (General) CSS File -->
</head>
<body>
<!-- Navigation Bar -->
<nav class="navbar navbar-inverse navbar-fixed-top no-bottom-margin" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-CANd-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/web-assignment/">α</a>
        </div>

        <div class="collapse navbar-collapse navbar-right" id="bs-CANd-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="/web-assignment/">Home</a></li>
                <li><a href="/web-assignment/about.php">About</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Programmes<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="http://nsbm.lk/programmes?type=all&category=2&stream=all">Undergraduate</a></li>
                        <li><a href="http://nsbm.lk/programmes?type=all&category=1&stream=all">Postgraduate</a></li>
                    </ul>
                </li>
                <li><a href="/web-assignment/contact.php">Contact</a></li>
                <?php
                    if(isset( $_SESSION['logged_in_user'])) {
                        ?>
                        <li><a href="/web-assignment/"><i class="glyphicon glyphicon-user right-margin-10px"></i>
                                <?php
                                $name = '';
                                if(isset( $_SESSION['user_first_name'])){
                                    $name .= ucfirst(substr( $_SESSION['user_first_name'],0,1));
                                    $name .= '. ';
                                }
                                if(isset( $_SESSION['user_last_name'])){
                                    $name .= $_SESSION['user_last_name'];

                                }
                                echo $name;
                                ?>
                            </a></li>
                        <li><a href="/web-assignment/logout.php"><i class="glyphicon glyphicon-log-out right-margin-10px"></i>Logout</a></li>
                        <?php
                    }else{
                ?>
                <li><a href="/web-assignment/signup.php"><i class="fa fa-user-plus fa-lg right-margin-10px"></i>SignUp</a></li>
                <li><a href="/web-assignment/login.php"><i class="fa fa-sign-in fa-lg right-margin-10px"></i>Login</a></li>

                <?php } ?>
            </ul>
        </div>
    </div>
</nav><!-- End of Navigation Bar -->
