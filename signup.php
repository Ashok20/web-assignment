<?php $title = 'SignUp'; include 'include.php'?>
<?php include 'db_connection.php'?>
<?php

$signup_errors = array();

if(isset($_POST['Submit'])) {

    $first_name = $_POST["feedback_fname"];
    $last_name = $_POST["feedback_lname"];
    $password = $_POST["feedback_password"];
    $email = $_POST["feedback_email"];
    $contact = $_POST["feedback_contact"];

    //check if email alreday used
    $sql = "SELECT id,email FROM users WHERE email = '$email'";

    $result = mysqli_query($conn,$sql);



    if($row = mysqli_fetch_row($result)){
        $signup_errors['email'] = 'Email already used.';
    }else {

        $sql = "INSERT INTO users (first_name,last_name,password,email,contact) VALUES('$first_name','$last_name','$password','$email','$contact')";

        if (mysqli_query($conn, $sql)) {
            echo "New record created successfully";
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($conn);
        }
    }

}
?>
<?php include 'header.php'; ?>

<!-- Starting of Form -->
<div class="container margin_top">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3 col-lg-6">
            <img src="images/signup_image.png">
        </div>
        <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3 col-lg-6">
            <form class="form-horizontal" role="form" name="feedback_form" method="post" action="signup.php" onsubmit="validate_text(event)">
                <div id="fname_division" class="form-group">
                    <label class="control-label col-sm-3" for="feedback_fname">First Name</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="feedback_fname">
                        <span id="feedback_fname_span" aria-hidden="true"></span>
                        <span id="helpBlock_fname" class="help-block display_none"></span>
                    </div>
                </div>
                <div id="lname_division" class="form-group">
                    <label class="control-label col-sm-3" for="feedback_lname">Last Name</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="feedback_lname">
                        <span id="feedback_lname_span" aria-hidden="true"></span>
                        <span id="helpBlock_lname" class="help-block display_none"></span>
                    </div>
                </div>
                <div id="password_division" class="form-group">
                    <label class="control-label col-sm-3" for="feedback_password">Password</label>
                    <div class="col-sm-9">
                        <input type="password" class="form-control" name="feedback_password">
                        <span id="feedback_password_span" aria-hidden="true"></span>
                        <span id="helpBlock_password" class="help-block display_none"></span>
                    </div>
                </div>
                <div id="re-password_division" class="form-group">
                    <label class="control-label col-sm-3" for="feedback_password">Confirm Password</label>
                    <div class="col-sm-9">
                        <input type="password" class="form-control" name="feedback_re-password">
                        <span id="feedback_re-password_span" aria-hidden="true"></span>
                        <span id="helpBlock_re-password" class="help-block display_none"></span>
                    </div>
                </div>
                <div id="email_division" class="form-group <?php if(isset( $signup_errors['email'])){echo 'has-error';} ?>">
                    <label class="control-label col-sm-3" for="feedback_email">Email</label>
                    <div class="col-sm-9">
                        <input type="email" class="form-control" name="feedback_email">
                        <span aria-hidden="true"></span>
                        <span id="feedback_email_status" class="sr-only">(success)</span>
                        <span id="helpBlock_email" class="help-block <?php if(isset( $signup_errors['email'])){echo '';}else{echo 'display_none';}?>"> <?php if(isset( $signup_errors['email'])){ echo $signup_errors['email'];} ?></span>
                    </div>
                </div>
                <div id="contact_division" class="form-group">
                    <label class="control-label col-sm-3" for="feedback_contact">Contact Number</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="feedback_contact">
                        <span aria-hidden="true"></span>
                        <span id="feedback_contact_status" class="sr-only">(success)</span>
                        <span id="helpBlock_contact" class="help-block display_none"></span>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-md btn pull-right" name="Submit">Sign up</button>
            </form>
        </div>
    </div>
</div><!-- End of Form -->

<?php include('footer.php') ?>
